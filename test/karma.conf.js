module.exports = function(config) {
  'use strict';

  config.set({
    basePath: '../',
    frameworks: ['jasmine'],

    files: [
      'test/dist/js/app.js',
    ],

    exclude: [
      'test/spec/spec_helper.js',
      'test/spec/angular-mocks.js',
    ],

    browsers: [
      "PhantomJS",
      //"Chrome",
    ],

    port: 8888,
    colors: true,
    singleRun: true,

    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,
  });
};
