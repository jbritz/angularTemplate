'use strict';

window.jQuery = require('jquery');
require('bootstrap');
require('angular/angular');
require('angular-ui-router');

var exampleApp = angular.module('exampleApp',
    [
      'ui.router',
      'homePage'
    ]);

exampleApp.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('homePage', {
    url: '/',
    templateUrl: 'homePage/homePage.html',
  });
});

module.exports = exampleApp;

