'use strict';

var homePage = angular.module('homePage', []);

homePage.controller('HomePageController',
    ['$scope', require('./homePageController.js')]);

module.exports = homePage;
