"use strict";

describe("HomePageController", function(){
  var homePageController = require('./homePageController.js');
  var $scope;

  beforeEach(function(){
    $scope = {};
  });

  it("assigns homePage to the scope", function(){
    homePageController($scope);

    expect($scope.homePage).toEqual("yessir");
  });
});
