# puts notes here

Git clone the repo. Obviously

Install:

    cd angularTemplate
    brew install node
    npm install
    npm install -g grunt-cli
    
Run the tests:

    grunt test
    
Boot the app:

    grunt boot

TODO's:
  - service discovery type root scope for angular?
  - figure out how to get angular/jquery out of the global scope
  - figure out how to split app.js up into the domain specific places