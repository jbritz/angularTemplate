'use strict';

module.exports = function(grunt) {
  var port = grunt.option('port') || '9001';
  grunt.initConfig({
    browserify: {
      js: {
        src: ['app/**/*.js', '!app/**/*.spec.js'],
        dest: 'dist/js/app.js',
      },
      spec: {
        src: 'app/**/*.spec.js',
        dest: 'test/dist/js/app.js',
      },
    },

    watch: {
      scripts: {
        files: ['app/**/*.js'],
        tasks: ['browserify'],
        options: {
          spawn: false,
        },
      },

      html: {
        files: ['app/**/*.html'],
        tasks: ['copy'],
        options: {
          spawn: false,
        },
      },

      css: {
        files: ['app/**/*.less'],
        tasks: ['less', 'copy'],
        options: {
          spawn: false,
        },
      },
    },

    copy: {
      all: {
        expand: true,
        cwd: 'app/',
        src: ['**/*.html', '**/*.css'],
        dest: 'dist/',
      },

      fonts: {
        expand: true,
        cwd: 'node_modules/bootstrap/dist/fonts',
        src: ['**/*.*'],
        dest: 'dist/fonts',
      },

      assets: {
        expand: true,
        cwd: 'assets/',
        src: ['**/*.jpeg', '**/*.png'],
        dest: 'dist/assets',
      }
    },

    connect: {
      server: {
        options: {
          port: port,
          base: 'dist',
        }
      },
    },

    less: {
      development: {
        options: {
          paths: ["app/"],
        },
        files: {
          "app/app.css": "app/app.less"
        }
      },
    },

    karma: {
      unit: {
        configFile: 'test/karma.conf.js'
      }
    },

    jshint: {
      options: {
        strict: true,
        browserify: true,
        globals: {
          "describe": false,
          "xdescribe": false,
          "ddescribe": false,
          "it": false,
          "xit": false,
          "iit": false,
          "beforeEach": false,
          "afterEach": false,
          "expect": false,
          "pending": false,
          "spyOn": false,
          "angular": false,
          "Microsoft": false,
          "inject": false,
          "jasmine": false,
        },
      },
      all: ['Gruntfile.js', 'app/**/*.js', 'test/spec/**/*.js'],
    },

    config: {
      dev: {
        domain: "http://localhost:8080",
      },
      test: {
        domain: "http://localhost:8081",
      },
    },

  });

  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerMultiTask('config', 'Create configs for various environments', function() {
    grunt.file.write('app/config.js', 'module.exports = ' + JSON.stringify(this.data) + ";");
  });

  grunt.registerTask('test', ['browserify', 'karma', 'jshint']);
  grunt.registerTask('boot', ['config:dev', 'browserify:js', 'less', 'copy', 'connect', 'watch']);
};
